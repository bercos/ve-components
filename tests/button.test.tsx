import * as React from 'react';
import { render, fireEvent, cleanup } from '@testing-library/react';
import 'jest-dom/extend-expect';
import { Button } from '../src/components';

afterEach(cleanup);

test('Button calls onClick', () => {
  const onClick = jest.fn();
  const { getByText } = render(<Button onClick={onClick}>Test</Button>);
  fireEvent.click(getByText('Test'));
  expect(onClick).toHaveBeenCalled();
});

test('Button calls onClick multiple times', () => {
  const onClick = jest.fn();
  const { getByText } = render(<Button onClick={onClick}>Test</Button>);
  fireEvent.click(getByText('Test'));
  fireEvent.click(getByText('Test'));
  fireEvent.click(getByText('Test'));
  expect(onClick).toHaveBeenCalledTimes(3);
});
