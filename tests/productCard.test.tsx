import * as React from 'react';
import { render, fireEvent, cleanup } from '@testing-library/react';
import 'jest-dom/extend-expect';
import { ProductCard } from '../src/components';

afterEach(cleanup);

test('Button calls onClick', () => {
  const onClick = jest.fn();
  const { getByText } = render(
    <ProductCard
      title={'LOAN (BETA)'}
      subTitle={'Analytics'}
      subscribed={true}
      onClick={onClick}
    />
  );
  fireEvent.click(getByText('LOAN (BETA)'));
  expect(onClick).toHaveBeenCalled();
});

test('Button calls onClick multiple times', () => {
  const onClick = jest.fn();
  const { getByText } = render(
    <ProductCard
      title={'LOAN (BETA)'}
      subTitle={'Analytics'}
      subscribed={false}
      onClick={onClick}
    />
  );
  fireEvent.click(getByText('LOAN (BETA)'));
  fireEvent.click(getByText('LOAN (BETA)'));
  fireEvent.click(getByText('LOAN (BETA)'));
  expect(onClick).toHaveBeenCalledTimes(3);
});
