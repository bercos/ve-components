# TODO

- Add new files to storybook
- Add tests
- Take H4 out of modal body

# Components to make

# Component Best Practices

## Reusable Components

- Always `forwardRef()`
  - If component is based off of some input type (button, input, select, etc.) forward to that
  - Otherwise use best judgement
- Always spread `{...rest}` props
  - If component is based off of some input type (button, input, select, etc.) spread on that
  - Otherwise use best judgement
