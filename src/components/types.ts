export type CustomChildren<P> =
  | (React.ReactElement<P> | React.ReactElement<P>[])
  | (React.ReactElement<P> | React.ReactElement<P>[])[];
