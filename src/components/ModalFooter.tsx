import React from 'react';
import styled from '@emotion/styled';

const ModalFooter = styled.div`
  background: #ecf0f1;
  border-radius: 0 0 6px 6px;
  padding: 19px 20px 20px;
  margin-top: 15px;
  text-align: right;
  border-top: 1px solid #e5e5e5;
`;

export { ModalFooter };
