import React from 'react';
import styled from '@emotion/styled';
import theme from './theme';

const Option = styled.option`
  padding: 3px 4px;
  box-sizing: border-box;
  color: #858585;
`;

const Select = styled.select`
  height: 34px;
  padding: 6px 12px;
  width: 100%;
  background-color: white;
  border: 1px solid #d5d5d5;
  color: #858585;
  &:focus {
    border-color: ${theme.colors.blue.normal};
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075),
      0 0 8px rgba(102, 175, 233, 0.6);
  }
`;

export { Select, Option };
