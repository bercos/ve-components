import React from 'react';
import styled from '@emotion/styled';
import theme from './theme';
import { css } from '@emotion/core';

interface CustomButtonProps {
  color?: 'blue' | 'orange';
  variant?: 'filled' | 'outlined';
}

const buttonFilled = ({ color, variant }: CustomButtonProps) => css`
  background-color: ${theme.colors[color].normal};
  color: white;
  border: 1px solid transparent;
  :hover {
    background-color: ${theme.colors[color].dark};
  }
  :active {
    background-color: ${theme.colors[color].darker};
  }
`;

const buttonOutlined = ({ color, variant }: CustomButtonProps) => css`
  background-color: white;
  color: ${theme.colors[color].normal};
  border: 1px solid ${theme.colors[color].normal};
  :hover {
    color: ${theme.colors[color].dark};
    border-color: ${theme.colors[color].dark};
  }
  :active {
    color: ${theme.colors[color].darker};
    border-color: ${theme.colors[color].darker};
  }
`;

const buttonStyles = (props: CustomButtonProps) =>
  props.variant === 'filled' ? buttonFilled : buttonOutlined;

const BaseButton = styled.button<CustomButtonProps>`
  width: auto;
  height: ${theme.button.height};
  font-size: 14px;
  ::-moz-focus-inner {
    border: 0;
  }
  transition: background-color 100ms ease-in-out, color 100ms ease-in-out,
    border-color 100ms ease-in-out;
  ${buttonStyles}
`;

BaseButton.defaultProps = {
  color: 'blue',
  variant: 'filled'
};

export { BaseButton as Button };
