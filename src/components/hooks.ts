import { useState, useEffect, MutableRefObject } from 'react';
import ResizeObserver from 'resize-observer-polyfill';

interface Measurements {
  left: number;
  top: number;
  width: number;
  height: number;
}

export const useMeasure = <T extends HTMLElement>(
  ref: MutableRefObject<T>
): Measurements => {
  const [bounds, set] = useState({ left: 0, top: 0, width: 0, height: 0 });
  const [ro] = useState(
    () => new ResizeObserver(([entry]) => set(entry.contentRect))
  );
  useEffect(() => (ro.observe(ref.current), ro.disconnect), []);
  return bounds;
};

// https://usehooks.com/useOnClickOutside/
export const useOnClickOutside = <T extends HTMLElement>(
  ref: MutableRefObject<T>,
  handler: (event: any) => void
) => {
  useEffect(() => {
    const listener = event => {
      // Do nothing if clicking ref's element or descendent elements
      if (!ref.current || ref.current.contains(event.target)) {
        return;
      }
      handler(event);
    };
    document.addEventListener('mousedown', listener);
    return () => {
      document.removeEventListener('mousedown', listener);
    };
  }, [ref, handler]);
};
