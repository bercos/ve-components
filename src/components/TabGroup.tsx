import React, { useRef, HTMLProps, useEffect, useReducer } from 'react';
import styled from '@emotion/styled';
import { useMeasure } from './hooks';
import { TabNav } from './TabNav';
import { CustomChildren } from './types';

// Tab Component
interface InternalTabProps {
  dark?: boolean;
  active?: boolean;
  onClick?: () => void;
}

type TabProps = InternalTabProps;

const OuterTab = styled.li<InternalTabProps>`
  position: relative;
  overflow: hidden;
`;

const InnerTab = styled.a<InternalTabProps>`
  width: 130px;
  display: block;
  text-align: center;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  color: ${({ active }) => (active ? 'rgb(136, 131, 119)' : '#999999')};
  background-color: ${({ active, dark }) =>
    active || dark ? '#EAEAEA' : '#FFF'};
  box-sizing: border-box;
  cursor: pointer;
  user-select: none;
  height: 36px;
  font-weight: ${({ active }) => (active ? 'bold' : 'normal')};
  padding: 10px 15px;
  :hover {
    background-color: #eaeaea;
    color: #888377;
  }
  :active {
    font-weight: bold;
    color: rgb(136, 131, 119);
    background-color: #eaeaea;
  }
`;

const Tab: React.FC<TabProps> = ({ children, ...rest }) => {
  return (
    <OuterTab {...rest}>
      <InnerTab {...rest}>{children}</InnerTab>
    </OuterTab>
  );
};

// Tab Group Component
export interface TabGroupProps {
  onChange?: (index: number) => void;
  children: CustomChildren<TabProps>;
  selectedIndex: number;
  borderBottom?: boolean;
  dark?: boolean;
}

const StyledDiv = styled.div<{ borderBottom?: boolean; dark?: boolean }>`
  display: grid;
  grid-template-columns: 1fr min-content;
  border: 1px solid #ddd;
  ${({ borderBottom }) => !borderBottom && 'border-bottom: 0;'}
  ${({ dark }) => dark && 'background-color: #EAEAEA;'}
  & > li {
    border-right: 1px solid #ddd;
  }
`;

const StyledTabContainer = styled.span<{ dark?: boolean }>`
  display: grid;
  grid-auto-flow: column;
  justify-content: start;
  & > li {
    border-right: 1px solid #ddd;
  }
`;

const getChildrenPerRow = (containerWidth: number): number => {
  return Math.floor((containerWidth - 110) / 130);
};

const isDisplayed = (
  index: number,
  offset: number,
  childrenPerRow: number
): boolean => {
  return index >= offset && index < offset + childrenPerRow;
};

const NEXT = 'NEXT';
const PREV = 'PREV';
const WIDTH_UPDATE = 'WIDTH_UPDATE';

const initialState: TabGroupState = {
  offset: 0,
  childrenPerRow: 0,
  prevDisabled: true,
  nextDisabled: true
};

const reducer = (state: TabGroupState, action): TabGroupState => {
  const { offset, childrenPerRow } = state;
  const { childCount } = action.payload;
  switch (action.type) {
    case PREV: {
      const { onChange } = action.payload;
      const newOffset = Math.max(0, offset - childrenPerRow);
      onChange(newOffset);
      return {
        ...state,
        offset: newOffset,
        prevDisabled: newOffset === 0,
        nextDisabled: newOffset + childrenPerRow >= childCount
      };
    }
    case NEXT: {
      const { onChange } = action.payload;
      const newOffset = Math.min(offset + childrenPerRow, childCount);
      onChange(newOffset);
      return {
        ...state,
        offset: newOffset,
        prevDisabled: newOffset === 0,
        nextDisabled: newOffset + childrenPerRow >= childCount
      };
    }
    case WIDTH_UPDATE: {
      const { selectedIndex } = action.payload;
      let newOffset = offset;
      const newChildrenPerRow = getChildrenPerRow(action.payload.width);
      if (
        newChildrenPerRow > 0 &&
        !isDisplayed(selectedIndex, newOffset, newChildrenPerRow)
      ) {
        newOffset = selectedIndex - (newChildrenPerRow - 1);
      }
      return {
        ...state,
        offset: newOffset,
        childrenPerRow: newChildrenPerRow,
        prevDisabled: newOffset === 0,
        nextDisabled: newOffset + newChildrenPerRow >= childCount
      };
    }
  }
};

// Offset is the first index showing
interface TabGroupState {
  offset: number;
  childrenPerRow: number;
  prevDisabled: boolean;
  nextDisabled: boolean;
}

const TabGroup: React.FC<TabGroupProps> = ({
  onChange,
  children,
  selectedIndex,
  borderBottom,
  dark
}) => {
  const ref = useRef<HTMLDivElement>();
  const { width } = useMeasure(ref);
  const childCount = React.Children.count(children);
  const [
    { offset, prevDisabled, nextDisabled, childrenPerRow },
    dispatch
  ] = useReducer(reducer, initialState);
  useEffect(() => {
    dispatch({
      type: WIDTH_UPDATE,
      payload: { selectedIndex, width, childCount }
    });
  }, [width]);
  const childrenToShow = React.Children.map(
    children,
    (child: React.ReactElement<TabProps>) => child
  ).filter((_, i) => isDisplayed(i, offset, childrenPerRow));
  return (
    <StyledDiv ref={ref} borderBottom={borderBottom} dark={dark}>
      <StyledTabContainer>
        <Tabs
          childrenToShow={childrenToShow}
          selectedIndex={selectedIndex}
          dark={dark}
          onChange={onChange}
          offset={offset}
        />
      </StyledTabContainer>
      <TabNav
        onPrevClick={() =>
          dispatch({ type: PREV, payload: { onChange, childCount } })
        }
        onNextClick={() =>
          dispatch({ type: NEXT, payload: { onChange, childCount } })
        }
        prevDisabled={prevDisabled}
        nextDisabled={nextDisabled}
      />
    </StyledDiv>
  );
};

interface TabsProps {
  childrenToShow: React.ReactElement<TabProps>[];
  selectedIndex: number;
  dark: boolean;
  onChange: (index: number) => void;
  offset: number;
}

const Tabs: React.FC<TabsProps> = ({
  childrenToShow,
  selectedIndex,
  dark,
  onChange,
  offset
}) => {
  return (
    <>
      {childrenToShow.map((child: React.ReactElement<TabProps>, index) =>
        React.cloneElement(child, {
          onClick: () => onChange(offset + index),
          active: selectedIndex === offset + index,
          dark: dark,
          ...child.props
        })
      )}
    </>
  );
};

TabGroup.defaultProps = {
  onChange: () => {},
  borderBottom: true,
  dark: false
};

export { TabGroup, Tab };
