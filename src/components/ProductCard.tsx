import React from 'react';
import styled from '@emotion/styled';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowCircleRight } from '@fortawesome/free-solid-svg-icons';

const CardContainer = styled.div`
  max-height: 250px;
  box-sizing: border-box;
`;

const ProductPanel = styled.div<Pick<ProductCardProps, 'subscribed'>>`
  cursor: pointer;
  text-align: right;
  background-color: ${({ subscribed }) => (subscribed ? '#82b9e3' : '#d0d0d0')};
  color: #fff;
  font-size: 26px;
  padding-bottom: 25px;
  margin-right: 10px;
  padding-left: 0;
  padding-top: 10px;
  height: 105px;
  box-sizing: border-box;
  padding-right: 15px;
  position: relative;
`;

const P = styled.p`
  text-align: right;
  margin: 10px 0;
  color: white;
  font-size: 14px;
`;

const SubscribeBar = styled.div<Pick<ProductCardProps, 'subscribed'>>`
  padding: 5px 5px;
  position: absolute;
  display: flex;
  justify-content: space-between;
  align-items: center;
  bottom: 0;
  text-align: left;
  font-size: 14px;
  font-weight: bold;
  background-color: ${({ subscribed }) => (subscribed ? '#68abdd' : '#c0c0c0')};
  box-sizing: border-box;
  width: 100%;
`;

interface ProductCardProps {
  title: string;
  subTitle: string;
  subscribed: boolean;
  onClick: () => void;
}

const ProductCard: React.FC<ProductCardProps> = ({
  title,
  subTitle,
  subscribed,
  onClick
}) => {
  return (
    <CardContainer onClick={() => onClick()}>
      <ProductPanel subscribed={subscribed}>
        {title}
        <P>{subTitle}</P>
        <SubscribeBar subscribed={subscribed}>
          {subscribed ? 'Subscribed' : 'Not Subscribed'}
          <FontAwesomeIcon color="white" icon={faArrowCircleRight} size="sm" />
        </SubscribeBar>
      </ProductPanel>
    </CardContainer>
  );
};

export { ProductCard };
