import React from 'react';
import styled from '@emotion/styled';
import { Portal } from './Portal';
import { animated, useTransition } from 'react-spring/web.cjs';
import { useOnClickOutside } from './hooks';

interface ModalProps {
  isOpen: boolean;
  onClose: () => void;
}

const BackgroundBlur = animated(
  styled.div<{ isOpen: boolean }>`
    z-index: 1000;
    position: fixed;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    width: 100%;
    height: 100%;
    background: rgba(0, 0, 0, 0.6);
    overflow: auto;
    pointer-events: ${({ isOpen }) => (isOpen ? 'all' : 'none')};
  `
);

const ModalDialog = animated(
  styled.div`
    width: 600px;
    box-sizing: border-box;
    padding: 30px 0;
    z-index: 1050;
    height: auto;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    margin-right: auto;
    margin-left: auto;
    box-sizing: border-box;
  `
);

const ModalContent = styled.div`
  background: white;
  border: 1px solid #999;
  border-radius: 6px;
  box-shadow: 0 5px 15px rgba(0, 0, 0, 0.5);
`;

const Modal: React.FC<ModalProps> = React.forwardRef(
  (
    { isOpen, children, onClose, ...rest },
    forwardRef: React.MutableRefObject<HTMLDivElement>
  ) => {
    const ref = forwardRef || React.useRef<HTMLDivElement>();
    useOnClickOutside(ref, onClose);
    const backgroundTransition = useTransition(isOpen, null, {
      from: {
        opacity: 0
      },
      enter: {
        opacity: 1
      },
      leave: {
        opacity: 0
      }
    });

    const positionTransition = useTransition(isOpen, null, {
      from: {
        transform: 'translate3d(0, -100%, 0)'
      },
      enter: {
        transform: 'translate3d(0, 0, 0)'
      },
      leave: {
        transform: 'translate3d(0, -100%, 0)'
      }
    });
    return (
      <Portal id={'modal-portal'}>
        {backgroundTransition.map(
          ({ item, key, props: animation }) =>
            item && (
              <BackgroundBlur
                ref={forwardRef}
                style={animation}
                key={key}
                isOpen={isOpen}
                {...rest}
              />
            )
        )}
        {positionTransition.map(
          ({ item, props: animation, key: key }) =>
            item && (
              <ModalDialog ref={ref} key={key} style={animation}>
                <ModalContent>{children}</ModalContent>
              </ModalDialog>
            )
        )}
      </Portal>
    );
  }
);

export { Modal };
