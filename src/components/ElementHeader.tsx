import React from 'react';
import styled from '@emotion/styled';
import theme from './theme';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars } from '@fortawesome/free-solid-svg-icons';

const StyledDiv = styled.div`
  background-color: ${theme.colors.blue.normal};
  display: grid;
  justify-content: space-between;
  grid-auto-flow: column;
  align-items: center;
  margin: 15px 0;
  padding: 10px;
  box-sizing: border-box;
  line-height: 22.85px;
`;

const TitleSpan = styled.span`
  font-weight: bold;
  color: white;
`;

const IconSpan = styled.span`
  cursor: pointer;
`;

interface ElementHeaderProps {
  title: string;
  onClick?: () => void;
}

const ElementHeader: React.FC<ElementHeaderProps> = React.forwardRef(
  (
    { title, onClick, ...rest },
    forwardRef: React.MutableRefObject<HTMLDivElement>
  ) => {
    return (
      <StyledDiv {...rest} ref={forwardRef}>
        <TitleSpan>{title}</TitleSpan>
        <IconSpan onClick={onClick}>
          <FontAwesomeIcon color="white" icon={faBars} size="lg" />
        </IconSpan>
      </StyledDiv>
    );
  }
);

ElementHeader.defaultProps = {
  onClick: () => {}
};

export { ElementHeader };
