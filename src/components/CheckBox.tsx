import React from 'react';
import styled from '@emotion/styled';

const CheckboxContainer = styled.span`
  display: grid;
  grid-template-columns: auto max-content;
  grid-column-gap: 10px;
  align-content: center;
  justify-content: start;
  user-select: none;
  width: auto;
  :hover {
    color: #67aadc;
  }
`;

export interface CheckBoxProps
  extends Omit<React.HTMLProps<HTMLInputElement>, 'onChange'> {
  checked: boolean;
  onChange: () => void;
  label?: string;
}

const CheckBox: React.FC<CheckBoxProps> = React.forwardRef(
  ({ checked, onChange, label, ...rest }, forwardRef) => {
    return (
      <CheckboxContainer>
        <input
          id={label}
          checked={checked}
          onChange={onChange}
          ref={forwardRef}
          {...rest}
          type="checkbox"
        />
        <label htmlFor={label}>{label}</label>
      </CheckboxContainer>
    );
  }
);

export { CheckBox };
