import React from 'react';
import styled from '@emotion/styled';

const ModalHeader = styled.div`
  border-top-right-radius: 5px;
  border-top-left-radius: 5px;
  background-color: #68abdd;
  color: #fff;
  min-height: 16px;
  padding: 15px;
  border-bottom: 1px solid #e5e5e5;
  font-weight: 700;
  font-size: 1em;
`;

export { ModalHeader };
