import React from 'react';
import styled from '@emotion/styled';

const TabPanel = styled.div`
  width: 100%;
  border: 1px solid #dddddd;
  border-top-width: 0px;
  min-height: 130px;
  padding: 16px 30px;
  box-sizing: border-box;
`;

export { TabPanel };
