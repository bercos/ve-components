import React from 'react';
import styled from '@emotion/styled';
import theme from './theme';

type ValueType = 'dollar' | 'ratio' | 'count';

const prefixPadding = (type: ValueType) => {
  switch (type) {
    case 'dollar':
      return '15px';
    case 'ratio':
      return '20px';
    default:
      return '5px';
  }
};

const prefix = (type: ValueType) => {
  switch (type) {
    case 'dollar':
      return '$';
    case 'ratio':
      return '%';
    default:
      return '';
  }
};

type StyledInputProps = Pick<TextInputProps, 'valueType'>;

const StyledInput = styled.input<StyledInputProps>`
  height: 34px;
  width: 100%;
  background: none;
  padding-left: ${({ valueType }) => prefixPadding(valueType)};
  box-sizing: border-box;
  border-radius: 0;
  border: 1px solid #ccc;
  transition: border-color ease-in-out 100ms;
  :focus {
    border-color: ${theme.colors.blue.normal};
  }
  ::placeholder {
    color: #999;
  }
`;

const Prefix = styled.span`
  height: 34px;
  display: grid;
  align-content: center;
  box-sizing: border-box;
  padding-left: 5px;
  width: 100%;
  color: #808080;
`;

const Container = styled.div`
  input[type='text'] {
    position: absolute;
  }
  position: relative;
  color: #808080;
  min-width: 100px;
`;

interface TextInputProps
  extends Omit<React.HTMLProps<HTMLInputElement>, 'onChange'> {
  valueType?: ValueType;
  onChange?: (value: string) => void;
}

const TextInput: React.FC<TextInputProps> = React.forwardRef(
  (
    { valueType, onChange, ...rest },
    forwardRef: React.MutableRefObject<HTMLInputElement>
  ) => {
    return (
      <Container>
        <StyledInput
          type="text"
          valueType={valueType}
          onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
            onChange(e.target.value)
          }
          ref={forwardRef}
          {...rest}
        />
        <Prefix>{prefix(valueType)}</Prefix>
      </Container>
    );
  }
);

TextInput.defaultProps = {
  valueType: 'count',
  onChange: () => {}
};

export { TextInput };
