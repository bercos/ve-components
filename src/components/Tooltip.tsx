import React from 'react';
import styled from '@emotion/styled';
import { useTooltip, TooltipPopup } from '@reach/tooltip';
import '@reach/tooltip/styles.css';
import { useTransition, animated } from 'react-spring/web.cjs';

interface TooltipProps {
  label: string;
}

const AnimatedTooltip = animated(styled(TooltipPopup)`
  background: hsla(0, 0%, 0%, 0.75);
  color: white;
  border: none;
  border-radius: 4px;
  padding: 0.5em 1em;
`);

const Tooltip: React.FC<TooltipProps> = ({ children, label, ...rest }) => {
  const [trigger, tooltip, isVisible] = useTooltip();
  const transitions = useTransition(isVisible ? tooltip : false, null, {
    from: { opacity: 0 },
    enter: { opacity: 1 },
    leave: { opacity: 0 },
    config: { mass: 1, tension: 500, friction: 40 }
  });
  return (
    <>
      {React.cloneElement(children as any, trigger)}

      {transitions.map(
        ({ item: tooltip, props: styles, key }) =>
          tooltip && (
            <AnimatedTooltip
              key={key}
              label={label}
              {...tooltip}
              {...rest}
              style={styles}
            />
          )
      )}
    </>
  );
};

export { Tooltip };
