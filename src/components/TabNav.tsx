import React from 'react';
import styled from '@emotion/styled';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faChevronLeft,
  faChevronRight
} from '@fortawesome/free-solid-svg-icons';

interface TabNavProps {
  prevDisabled: boolean;
  nextDisabled: boolean;
  onPrevClick: () => void;
  onNextClick: () => void;
  className?: string;
}

const StyledSpan = styled.span`
  width: 110px;
  display: grid;
  grid-auto-flow: column;
  align-content: center;
  user-select: none;
`;

const LeftIconContainer = styled.span<{ disabled: boolean }>`
  cursor: pointer;
  color: ${({ disabled }) => (disabled ? '#999' : '#68abdd')};
  opacity: ${({ disabled }) => (disabled ? 0.4 : 1.0)};
  margin-right: 5px;
`;

const RightIconContainer = styled(LeftIconContainer)`
  /* margin-right: 0px; */
  margin-left: 5px;
`;

const TabNav: React.FC<TabNavProps> = ({
  prevDisabled,
  nextDisabled,
  onPrevClick,
  onNextClick,
  className
}) => {
  return (
    <StyledSpan className={className}>
      <LeftIconContainer
        onClick={prevDisabled ? () => {} : onPrevClick}
        disabled={prevDisabled}
      >
        <FontAwesomeIcon icon={faChevronLeft} />
        Prev
      </LeftIconContainer>
      <span>|</span>
      <RightIconContainer
        onClick={nextDisabled ? () => {} : onNextClick}
        disabled={nextDisabled}
      >
        Next
        <FontAwesomeIcon icon={faChevronRight} />
      </RightIconContainer>
    </StyledSpan>
  );
};

export { TabNav };
