const theme = {
  colors: {
    blue: {
      lighter: '#eaf3fa',
      light: '#d5e8f6',
      normal: '#68abdd',
      dark: '#4496d5',
      darker: '#2f8ad0'
    },
    orange: {
      lighter: '#fef0e7',
      light: '#fce1cf',
      normal: '#F37722',
      dark: '#da5e0b',
      darker: '#c2540a'
    },
    white: {
      normal: '#fff',
      dark: '#e6e6e6',
      darker: '#d9d9d9'
    }
  },
  button: {
    height: '34px'
  }
};

export default theme;
