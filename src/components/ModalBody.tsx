import React from 'react';
import styled from '@emotion/styled';

interface ModalBodyProps {
  text: string;
}

const ModalBodyContainer = styled.div`
  text-align: center;
  padding: 20px;
`;

const H4 = styled.h4`
  font-size: 18px;
  font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
  font-weight: 500;
  line-height: 19.8px;
`;

const ModalBody: React.FC<ModalBodyProps> = React.forwardRef(
  ({ text, ...rest }, forwardRef: React.MutableRefObject<HTMLDivElement>) => {
    return (
      <ModalBodyContainer ref={forwardRef} {...rest}>
        <H4>{text}</H4>
      </ModalBodyContainer>
    );
  }
);

export { ModalBody };
