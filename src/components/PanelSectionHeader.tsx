import React from 'react';
import styled from '@emotion/styled';

interface PanelSectionHeaderProps {
  text: string;
}

const TextSpan = styled.span`
  font-weight: bold;
  display: grid;
  align-items: center;
  justify-items: start;
  margin-bottom: 5px;
`;

const PanelSectionHeader: React.FC<PanelSectionHeaderProps> = ({
  text,
  ...rest
}) => {
  return <TextSpan {...rest}>{text}</TextSpan>;
};

export { PanelSectionHeader };
