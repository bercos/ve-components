import React from 'react';
import styled from '@emotion/styled';
import theme from './theme';

interface SelectDeselectAllProps {
  onSelect: () => void;
  onDeselect: () => void;
}

const Container = styled.span`
  color: ${theme.colors.blue.normal};
  user-select: none;
  display: grid;
  align-items: center;
  justify-items: space-between;
  grid-template-columns: max-content max-content max-content;
  grid-column-gap: 15px;
  margin-bottom: 10px;
`;

const WordSpan = styled.span`
  cursor: pointer;
  &:hover {
    color: ${theme.colors.blue.dark};
  }
`;

const SelectDeselectAll: React.FC<SelectDeselectAllProps> = React.forwardRef(
  (
    { onSelect, onDeselect, ...rest },
    forwardRef: React.MutableRefObject<HTMLSpanElement>
  ) => {
    return (
      <Container ref={forwardRef} {...rest}>
        <WordSpan onClick={onSelect}>Select All</WordSpan>
        <span>|</span>
        <WordSpan onClick={onDeselect}>Deselect All</WordSpan>
      </Container>
    );
  }
);

export { SelectDeselectAll };
