import React from 'react';
import styled from '@emotion/styled';
import RBTable from 'react-bootstrap/Table';
import 'bootstrap/dist/css/bootstrap.min.css';

export interface TableJson {
  rowClass?: string;
  colClass?: string;
  cssId?: string;
  hoverText?: string;
  data?: Record<string, string>;
  value?: string;
  href?: string;
  rows?: TableJson[];
  cols?: TableJson[];
}

interface TableProps {
  tableJson: TableJson;
}

interface HTMLAttrs {
  className?: string;
  id?: string;
}

const mapToHtmlProps = ({
  rowClass,
  colClass,
  cssId,
  data
}: TableJson): HTMLAttrs => {
  const attrs: HTMLAttrs = {};
  if (rowClass || colClass) {
    attrs.className = [rowClass, colClass].filter(Boolean).join(' ');
  }
  if (cssId) {
    attrs.id = cssId;
  }
  if (data) {
    Object.entries(data).forEach(
      ([key, value]) => value && (attrs[`data-${key}`] = value)
    );
  }
  console.log(attrs);
  return attrs;
};

const Table: React.FC<TableProps> = ({ tableJson }) => {
  const headerRow = tableJson.rows![0];
  const headerSize = headerRow.cols.length;
  const otherRows = tableJson.rows!.filter((_, i) => i !== 0);
  let i = 0;
  return (
    <RBTable hover>
      <thead>
        <tr>
          {headerRow.cols!.map(col => (
            <th key={i++} {...mapToHtmlProps(col)}>
              {col.rows!.map(row => row.value).join('</br>')}
            </th>
          ))}
        </tr>
      </thead>
      <tbody>
        {otherRows.map(row => (
          <Row row={row} totalCols={headerSize} />
        ))}
      </tbody>
    </RBTable>
  );
};

const Row: React.FC<{ row: TableJson; totalCols: number }> = ({
  row,
  totalCols
}) => {
  let i = 0;
  let extraRows = [];
  for (let j = 0; j < totalCols - row.cols.length; j++) {
    extraRows.push(i);
  }
  return (
    <tr {...mapToHtmlProps(row)}>
      {row.cols!.map(col => (
        <td key={i++} {...mapToHtmlProps(col)}>
          {col.rows!.map(colRow => (
            <span {...mapToHtmlProps(colRow)}>{colRow.value}</span>
          ))}
        </td>
      ))}
      {extraRows.length > 0 ? extraRows.map(() => <td />) : null}
    </tr>
  );
};

export { Table };
