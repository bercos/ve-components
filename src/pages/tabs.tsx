import React, { useState } from 'react';
import { render } from 'react-dom';
import styled from '@emotion/styled';
import { TabStructure } from './tabs/TabStructure';
import { tabJson, values } from './tabs/tabJson';
import { TabValuesProvider } from './tabs/TabValuesContext';
import { FilterSummary } from './tabs/FilterSummary';

const MainContainer = styled.div`
  padding-left: calc(2% + 15px);
  padding-right: calc(2% + 15px);
  display: grid;
  grid-template-columns: 3fr 1fr;
`;

render(
  <TabValuesProvider values={values}>
    <MainContainer>
      <TabStructure tabJson={tabJson} values={values} />
      <FilterSummary />
    </MainContainer>
  </TabValuesProvider>,
  document.getElementById('root')
);
