import React from 'react';
import ReactDOM from 'react-dom';
import styled from '@emotion/styled';
import {
  Button,
  TextInput,
  ElementHeader,
  CheckBox,
  TabGroup,
  Tab,
  SelectDeselectAll,
  Select,
  Option,
  PanelSectionHeader,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Tooltip,
  ProductCard,
  TabPanel
} from '../components/index';
import { css } from '@emotion/core';

const ButtonsDiv = styled.div`
  display: grid;
  grid-auto-columns: max-content;
  grid-auto-flow: column;
  grid-column-gap: 10px;
`;

const HeadersDiv = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-column-gap: 15px;
`;

const ProductDiv = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  grid-column-gap: 15px;
`;

const nums = [];
for (let i = 0; i < 14; i++) {
  nums.push(i);
}

const App = () => {
  const [checked, setChecked] = React.useState(true);
  const [selectedIndex, setSelectedIndex] = React.useState(0);
  const [i, seti] = React.useState(12);
  const [modalOpen, setModalOpen] = React.useState(false);
  return (
    <div>
      <ButtonsDiv>
        <Button color="blue" variant="outlined">
          Actions
        </Button>
        <Button color="orange" variant="outlined">
          Actions
        </Button>
        <Button color="blue" variant="filled">
          Actions
        </Button>
        <Button color="orange" variant="filled">
          Actions
        </Button>
        <TextInput onChange={value => console.log(value)} />
        <TextInput valueType="dollar" />
        <TextInput valueType="ratio" />
        <CheckBox
          label="Business Lending"
          onChange={() => setChecked(!checked)}
          checked={checked}
        />
        <SelectDeselectAll onSelect={() => {}} onDeselect={() => {}} />
        <Select value={0} onChange={console.log}>
          {nums.map(num => (
            <Option key={num} disabled={num === 1} value={num}>
              {num}
            </Option>
          ))}
          <Option value="a">a</Option>
        </Select>
        <PanelSectionHeader text="Standard Category" />
      </ButtonsDiv>
      <HeadersDiv>
        <ElementHeader title="KEY STATS" />
        <ElementHeader title="KEY STATS" />
        <ElementHeader title="KEY STATS" />
      </HeadersDiv>
      <TabGroup
        selectedIndex={selectedIndex}
        onChange={index => setSelectedIndex(index)}
        borderBottom={false}
      >
        <Tab>Common</Tab>
        <Tab>Not Common</Tab>
        <Tab>Uncommon</Tab>
      </TabGroup>
      <TabGroup selectedIndex={i} onChange={index => seti(index)} dark>
        {nums.map(num => (
          <Tab key={num}>{num}</Tab>
        ))}
      </TabGroup>
      <TabPanel>Hello</TabPanel>
      <Button
        color="blue"
        variant="outlined"
        onClick={() => setModalOpen(!modalOpen)}
      >
        Open
      </Button>
      <Modal isOpen={modalOpen} onClose={() => setModalOpen(false)}>
        <ModalHeader>Save Filter</ModalHeader>
        <ModalBody text={'Hello Bernie. You have 2 unread notifications.'} />
        <ModalFooter>
          <Button onClick={() => setModalOpen(false)}>View Now</Button>
          <Button
            css={css`
              margin-left: 5px;
            `}
            variant="outlined"
            onClick={() => setModalOpen(false)}
          >
            View Later
          </Button>
        </ModalFooter>
      </Modal>
      <Tooltip label="Hello">
        <CheckBox onChange={console.log} checked={true} />
      </Tooltip>
      <ProductDiv>
        <ProductCard
          title={'LOAN (BETA)'}
          subTitle={'Analytics'}
          subscribed={false}
          onClick={() => console.log('Click Not Subscribed')}
        />
        <ProductCard
          title={'LOAN (BETA)'}
          subTitle={'Analytics'}
          subscribed={true}
          onClick={() => console.log('Click Subscribed')}
        />
      </ProductDiv>
    </div>
  );
};

ReactDOM.render(<App />, document.getElementById('root'));
