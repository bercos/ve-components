import React, { useState, useRef } from 'react';
import styled from '@emotion/styled';
import Draggable from 'react-draggable';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes, faBug, faCopy } from '@fortawesome/free-solid-svg-icons';
import { Button } from '../../components/Button';
import theme from '../../components/theme';
import { values } from '../tabs/tabJson';
import { Light as SyntaxHighlighter } from 'react-syntax-highlighter';
import js from 'react-syntax-highlighter/dist/esm/languages/hljs/javascript';
import docco from 'react-syntax-highlighter/dist/esm/styles/hljs/docco';

SyntaxHighlighter.registerLanguage('javascript', js);

interface DebugPanelProps {}

const PanelContainer = styled.div`
  box-shadow: 0px 1px 3px 0px rgba(0, 0, 0, 0.2),
    0px 1px 1px 0px rgba(0, 0, 0, 0.14), 0px 2px 1px -1px rgba(0, 0, 0, 0.12);
  position: fixed;
  left: calc(50% - 250px);
  top: calc(50% - 250px);
`;

const Panel = styled.div`
  padding: 10px;
  max-height: 100vh;
  max-width: 100vw;
  height: 500px;
  width: 500px;
  display: flex;
  flex-direction: column;
`;

const Buttons = styled.div`
  display: grid;
  grid-auto-flow: column;
  grid-column-gap: 5px;
`;

const Header = styled.div`
  display: grid;
  grid-auto-flow: column;
  justify-content: end;
  align-content: center;
  background-color: ${theme.colors.orange.normal};
`;

const FAB = styled(Button)`
  position: fixed;
  bottom: 10px;
  right: 10px;
  border-radius: 50%;
`;

let arr = [];
let a = arr;

for (let i = 0; i < 50; i++) {
  arr.push([]);
  arr = arr[0];
}
arr.push(0);
arr = a;

const DebugPanel: React.FC<DebugPanelProps> = props => {
  const [open, setOpen] = useState(true);
  return (
    <>
      <FAB onClick={() => setOpen(!open)} color="orange">
        <FontAwesomeIcon icon={faBug} size="lg" />
      </FAB>
      {open && (
        <Draggable handle=".handle">
          <PanelContainer>
            <PanelHeader onClick={() => setOpen(false)} />
            <Panel>
              <Buttons>
                <Button>Settings Json</Button>
                <Button>Query Strings</Button>
                <Button>Settings Json</Button>
              </Buttons>
              <CodeBlock
                value={JSON.stringify(values, null, 2)}
                language={'json'}
              />
            </Panel>
          </PanelContainer>
        </Draggable>
      )}
    </>
  );
};

const PreContainer = styled.div`
  position: relative;
  width: 100%;
  display: flex;
  min-height: 0;
`;

const InnerPreContainer = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
`;

const CopyButton = styled(Button)`
  position: absolute;
  top: 15px;
  right: 5px;
  border-radius: 50%;
  cursor: pointer;
`;

interface CodeBlockProps {
  value: string;
  language: string;
}

const CodeBlock: React.FC<CodeBlockProps> = ({ language, value }) => {
  function copyToClipboard() {
    var dummy = document.createElement('textarea');
    // to avoid breaking orgain page when copying more words
    // cant copy when adding below this code
    // dummy.style.display = 'none'
    document.body.appendChild(dummy);
    //Be careful if you use texarea. setAttribute('value', value), which works with "input" does not work with "textarea". – Eduard
    dummy.value = value;
    dummy.select();
    document.execCommand('copy');
    document.body.removeChild(dummy);
  }
  return (
    <PreContainer>
      <CopyButton color="blue" variant="filled" onClick={copyToClipboard}>
        <FontAwesomeIcon icon={faCopy} size="lg" />
      </CopyButton>
      <InnerPreContainer>
        <SyntaxHighlighter css={{ flexGrow: 1 }} language={language} style={docco}>{value}</SyntaxHighlighter>
      </InnerPreContainer>
    </PreContainer>
  );
};

interface PanelHeaderProps {
  onClick: () => void;
}

const PanelHeader: React.FC<PanelHeaderProps> = ({ onClick }) => (
  <Header className="handle">
    <span css={{ paddingRight: 3 }} onClick={onClick}>
      <FontAwesomeIcon icon={faTimes} size="sm" />
    </span>
  </Header>
);

export { DebugPanel };
