import React from 'react';
import { render } from 'react-dom';
import { Table, TableJson, ElementHeader } from '@components';
import styled from '@emotion/styled';
import '@emotion/core';

const Center = styled.div`
  display: grid;
  grid-template-areas: '. c .';
  padding: 10px;
`;

const sample: TableJson = {
  rows: [
    {
      cols: [
        { rows: [{ value: 'Segmentation' }] },
        { rows: [{ value: 'Individually Reviewed Impaired Count' }] },
        { rows: [{ value: 'Individually Reviewed Impaired Balance' }] },
        { rows: [{ value: 'Individually Reviewed Allowance' }] },
        { rows: [{ value: 'Individually Reviewed Allowance Ratio' }] }
      ]
    },
    {
      cols: [
        {
          rows: [
            { rowClass: 'boldText', value: 'Commercial Real Estate - Retail' }
          ]
        },
        { rows: [{ value: '6' }] },
        { rows: [{ value: '$105,713' }] },
        { rows: [{ value: '$47,618' }] },
        { rows: [{ value: '45.04%' }] }
      ]
    },
    {
      cols: [
        { rows: [{ rowClass: 'boldText', value: 'Consumer Secured' }] },
        { rows: [{ value: '1' }] },
        { rows: [{ value: '$0' }] },
        { rows: [{ value: '$0' }] },
        { rows: [{ value: '0%' }] }
      ]
    },
    {
      cols: [
        { rows: [{ rowClass: 'boldText', value: 'Consumer Unsecured' }] },
        { rows: [{ value: '48' }] },
        { rows: [{ value: '$181,941' }] },
        { rows: [{ value: '$24,149' }] },
        { rows: [{ value: '13.27%' }] }
      ]
    },
    {
      cols: [
        { rows: [{ rowClass: 'boldText', value: 'Credit Card' }] },
        { rows: [{ value: '72' }] },
        { rows: [{ value: '$254,424' }] },
        { rows: [{ value: '$0' }] },
        { rows: [{ value: '0%' }] }
      ]
    },
    {
      cols: [
        { rows: [{ rowClass: 'boldText', value: 'First Mortgages' }] },
        { rows: [{ value: '7' }] },
        { rows: [{ value: '$615,457' }] },
        { rows: [{ value: '$18,817' }] },
        { rows: [{ value: '3.06%' }] }
      ]
    },
    {
      cols: [
        { rows: [{ rowClass: 'boldText', value: 'HELOC' }] },
        { rows: [{ value: '11' }] },
        { rows: [{ value: '$0' }] },
        { rows: [{ value: '$0' }] },
        { rows: [{ value: '0%' }] }
      ]
    },
    { cols: [{ rows: [{ rowClass: 'boldText', value: 'New Auto' }] }] },
    {
      cols: [
        { rows: [{ rowClass: 'boldText', value: '&emsp;A+' }] },
        { rows: [{ value: '0' }] },
        { rows: [{ value: '$0' }] },
        { rows: [{ value: '$0' }] },
        { rows: [{ value: '0%' }] }
      ]
    },
    {
      cols: [
        { rows: [{ rowClass: 'boldText', value: '&emsp;A' }] },
        { rows: [{ value: '0' }] },
        { rows: [{ value: '$0' }] },
        { rows: [{ value: '$0' }] },
        { rows: [{ value: '0%' }] }
      ]
    },
    {
      cols: [
        { rows: [{ rowClass: 'boldText', value: '&emsp;B' }] },
        { rows: [{ value: '0' }] },
        { rows: [{ value: '$0' }] },
        { rows: [{ value: '$0' }] },
        { rows: [{ value: '0%' }] }
      ]
    },
    {
      cols: [
        { rows: [{ rowClass: 'boldText', value: '&emsp;C' }] },
        { rows: [{ value: '0' }] },
        { rows: [{ value: '$0' }] },
        { rows: [{ value: '$0' }] },
        { rows: [{ value: '0%' }] }
      ]
    },
    {
      cols: [
        { rows: [{ rowClass: 'boldText', value: '&emsp;D' }] },
        { rows: [{ value: '0' }] },
        { rows: [{ value: '$0' }] },
        { rows: [{ value: '$0' }] },
        { rows: [{ value: '0%' }] }
      ]
    },
    {
      cols: [
        { rows: [{ rowClass: 'boldText', value: '&emsp;E' }] },
        { rows: [{ value: '6' }] },
        { rows: [{ value: '$25,487' }] },
        { rows: [{ value: '$0' }] },
        { rows: [{ value: '0%' }] }
      ]
    },
    {
      cols: [
        { rows: [{ rowClass: 'boldText', value: '&emsp;NS' }] },
        { rows: [{ value: '0' }] },
        { rows: [{ value: '$0' }] },
        { rows: [{ value: '$0' }] },
        { rows: [{ value: '0%' }] }
      ]
    },
    {
      cols: [
        {
          rows: [{ rowClass: 'boldText', value: 'Used Auto : No active loans' }]
        }
      ]
    },
    {
      cols: [
        { rows: [{ rowClass: 'boldText', value: 'Not Reported' }] },
        { rows: [{ value: '63' }] },
        { rows: [{ value: '$1,129,772' }] },
        { rows: [{ value: '$245,844' }] },
        { rows: [{ value: '21.76%' }] }
      ]
    }
  ]
};

render(
  <Center>
    <div css={{ gridArea: 'c' }}>
      <ElementHeader title="Summary" />
      <Table tableJson={sample} />
    </div>
  </Center>,
  document.getElementById('root')
);
