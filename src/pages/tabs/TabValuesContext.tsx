import React, { createContext, useContext, useState } from 'react';

type TabValuesContextValue = {
  values: Record<string, string[]>;
  update: TabValueUpdater;
};

const TabValuesContext = createContext<TabValuesContextValue | undefined>(
  undefined
);

interface TabValuesProviderProps {
  values: Record<string, string[]>;
}

const TabValuesProvider: React.FC<TabValuesProviderProps> = ({
  children,
  values
}) => {
  const [tabValues, setTabValues] = useState(values);
  return (
    <TabValuesContext.Provider
      value={{
        values: tabValues,
        update: (key, values) => setTabValues({ ...tabValues, [key]: values })
      }}
    >
      {children}
    </TabValuesContext.Provider>
  );
};

type TabValueUpdater = (key: string, values: string[]) => void;

const useTabValues = (): TabValuesContextValue => {
  const context = useContext(TabValuesContext);
  if (context === undefined) {
    throw new Error('useTabValues must be used within a TabValuesProvider');
  }
  return context;
};

export { TabValuesProvider, useTabValues };
