export type TabType = { name: string; subTabs: SubTab[] };
export type SubTab = {
  name: string;
  fields: (CheckboxField | TextField | SelectField | LookbackField)[];
};
export interface BaseField {
  name: string;
  type: 'text' | 'select' | 'checkbox' | 'lookback';
}
export interface CheckboxOption {
  label: string;
  value: string;
  selected: boolean;
}
export interface CheckboxField extends BaseField {
  type: 'checkbox';
  options: CheckboxOption[];
}
export interface TextInput {
  value: string;
  valueType: 'dollar' | 'count' | 'ratio';
  placeHolder: string;
}
export interface TextField extends BaseField {
  type: 'text';
  inputs: TextInput[];
}
export interface SelectOption {
  label: string;
  value: string;
}
export interface SelectField extends BaseField {
  type: 'select';
  value: string;
  options: SelectOption[];
}
export interface LookbackField extends BaseField {
  type: 'lookback';
  inputs: TextInput[];
  options: SelectOption[];
}
export const tabJson: TabType[] = [
  {
    name: 'Common',
    subTabs: [
      {
        name: 'Categories',
        fields: [
          {
            name: 'Standard Category',
            type: 'checkbox',
            options: [
              {
                label: 'Business Lending',
                value: 'Business Lending',
                selected: false
              },
              {
                label: 'Commercial Real Estate',
                value: 'Commercial Real Estate',
                selected: true
              },
              {
                label: 'Consumer Lending',
                value: 'Consumer Lending',
                selected: false
              },
              {
                label: 'Residential Real Estate',
                value: 'Residential Real Estate',
                selected: false
              }
            ]
          },
          {
            name: 'Standard Sub-Category',
            type: 'checkbox',
            options: [
              {
                label: 'Apartment',
                value: 'Apartment',
                selected: false
              },
              {
                label: 'Bus. Credit Card',
                value: 'Bus. Credit Card',
                selected: false
              },
              {
                label: 'Bus. Secured',
                value: 'Bus. Secured',
                selected: false
              },
              {
                label: 'Cons. Credit Card',
                value: 'Cons. Credit Card',
                selected: true
              },
              {
                label: 'Cons. New Auto',
                value: 'Cons. New Auto',
                selected: false
              },
              {
                label: 'Cons. Used Auto',
                value: 'Cons. Used Auto',
                selected: false
              },
              {
                label: 'First Mortgage',
                value: 'First Mortgage',
                selected: false
              },
              {
                label: 'HELOC',
                value: 'HELOC',
                selected: false
              },
              {
                label: 'Other Business',
                value: 'Other Business',
                selected: false
              },
              {
                label: 'Other Sec. Cons.',
                value: 'Other Sec. Cons.',
                selected: false
              },
              {
                label: 'Other Unsec. Cons.',
                value: 'Other Unsec. Cons.',
                selected: false
              },
              {
                label: 'SBA',
                value: 'SBA',
                selected: false
              },
              {
                label: 'Second Mortgage',
                value: 'Second Mortgage',
                selected: false
              }
            ]
          }
        ]
      },
      {
        name: 'Loan Types',
        fields: []
      },
      {
        name: 'Characteristics',
        fields: [
          {
            name: 'Balance',
            type: 'text',
            inputs: [
              {
                value: '1000',
                valueType: 'dollar',
                placeHolder: 'From:'
              },
              {
                value: '2000',
                valueType: 'dollar',
                placeHolder: 'To:'
              }
            ]
          },
          {
            name: 'Charge-off Balance',
            type: 'text',
            inputs: [
              {
                value: '',
                valueType: 'dollar',
                placeHolder: 'From:'
              },
              {
                value: '',
                valueType: 'dollar',
                placeHolder: 'To:'
              }
            ]
          },
          {
            name: 'Collateral Value',
            type: 'text',
            inputs: [
              {
                value: '',
                valueType: 'dollar',
                placeHolder: 'From:'
              },
              {
                value: '',
                valueType: 'dollar',
                placeHolder: 'To:'
              }
            ]
          },
          {
            name: 'Credit Limit',
            type: 'text',
            inputs: [
              {
                value: '',
                valueType: 'dollar',
                placeHolder: 'From:'
              },
              {
                value: '',
                valueType: 'dollar',
                placeHolder: 'To:'
              }
            ]
          },
          {
            name: 'Credit Score',
            type: 'text',
            inputs: [
              {
                value: '',
                valueType: 'count',
                placeHolder: 'From:'
              },
              {
                value: '',
                valueType: 'count',
                placeHolder: 'To:'
              }
            ]
          },
          {
            name: 'Days Delinquent',
            type: 'text',
            inputs: [
              {
                value: '',
                valueType: 'count',
                placeHolder: 'From:'
              },
              {
                value: '',
                valueType: 'count',
                placeHolder: 'To:'
              }
            ]
          },
          {
            name: 'Indirect Indicator',
            type: 'select',
            value: 'Not Selected',
            options: [
              {
                label: 'Include Indirect Loans',
                value: 'Include Indirect Loans'
              },
              {
                label: 'Only Indirect Loans',
                value: 'Only Indirect Loans'
              },
              {
                label: 'Exclude Indirect Loans',
                value: 'Exclude Indirect Loans'
              },
              {
                label: 'Not Selected',
                value: 'Not Selected'
              }
            ]
          },
          {
            name: 'LTV',
            type: 'text',
            inputs: [
              {
                value: '',
                valueType: 'ratio',
                placeHolder: 'From:'
              },
              {
                value: '',
                valueType: 'ratio',
                placeHolder: 'To:'
              }
            ]
          },
          {
            name: 'Maturity Date',
            type: 'text',
            inputs: [
              {
                value: '',
                valueType: 'count',
                placeHolder: 'From:'
              },
              {
                value: '',
                valueType: 'count',
                placeHolder: 'To:'
              }
            ]
          },
          {
            name: 'Maturity Date Lookback',
            type: 'lookback',
            inputs: [
              {
                value: '',
                valueType: 'count',
                placeHolder: 'From:'
              },
              {
                value: '',
                valueType: 'count',
                placeHolder: 'To:'
              }
            ],
            options: [
              {
                label: 'Month',
                value: 'MONTH'
              },
              {
                label: 'Quarter',
                value: 'QUARTER'
              },
              {
                label: 'Year',
                value: 'YEAR'
              }
            ]
          }
        ]
      }
    ]
  },
  {
    name: 'Borrower',
    subTabs: [
      {
        name: 'Characteristics',
        fields: []
      },
      {
        name: 'Geography',
        fields: []
      }
    ]
  },
  {
    name: 'Categorization',
    subTabs: [
      {
        name: 'Category',
        fields: []
      },
      {
        name: 'Sub-Category',
        fields: []
      },
      {
        name: 'Loan Type Code',
        fields: []
      },
      {
        name: 'Loan Type Description',
        fields: []
      },
      {
        name: 'Class',
        fields: []
      },
      {
        name: 'Purpose Code',
        fields: []
      }
    ]
  },
  {
    name: 'Characteristics',
    subTabs: [
      {
        name: 'Balances',
        fields: []
      },
      {
        name: 'Statistics',
        fields: []
      },
      {
        name: 'Auto Dealer',
        fields: []
      },
      {
        name: 'Branch',
        fields: []
      },
      {
        name: 'Loan Officer',
        fields: []
      },
      {
        name: 'Loan Underwriter',
        fields: []
      }
    ]
  },
  {
    name: 'Charge-off',
    subTabs: [
      {
        name: 'Balances',
        fields: []
      }
    ]
  },
  {
    name: 'Collateral',
    subTabs: [
      {
        name: 'Statistics',
        fields: []
      },
      {
        name: 'Balances',
        fields: []
      },
      {
        name: 'Auto Make',
        fields: []
      },
      {
        name: 'Auto Model',
        fields: []
      },
      {
        name: 'Auto Year',
        fields: []
      }
    ]
  }
];

export const values: Record<string, string[]> = {
  'Standard Category': ['Consumer Lending', 'Residential Real Estate'],
  'Indirect Indicator': ['Exclude Indirect Loans']
};
