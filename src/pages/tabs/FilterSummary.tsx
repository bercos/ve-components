import React from 'react';
import styled from '@emotion/styled';
import { Button } from '../../components/index';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencilAlt, faTimes } from '@fortawesome/free-solid-svg-icons';
import { useTabValues } from './TabValuesContext';

const FilterSummaryContainer = styled.div`
  min-height: 203px;
  padding: 0 15px;
`;

const Header = styled.div`
  background-color: #63605c;
  color: #fff;
  padding: 5px 15px;
  font-size: 15px;
`;

const SummaryContainer = styled.div`
  background-color: #f3f3f1;
  padding: 15px;
`;

const ActionsContainer = styled.div`
  display: grid;
  justify-content: space-between;
  grid-template-columns: auto auto;
`;

const FixedWidthButton = styled(Button)`
  width: 110px;
`;

const SummaryTotals = styled.div`
  padding-top: 15px;
  margin-left: 10px;
`;

const TotalContainer = styled.span`
  display: grid;
  grid-template-columns: auto auto;
  justify-content: start;
  grid-column-gap: 5px;
`;

const P = styled.p`
  font-size: 16px;
  margin: 0 0 10px 0;
  &:nth-of-type(1) {
    color: #808080;
  }
  &:nth-of-type(2) {
    color: #68abdd;
  }
`;

interface FilterSummaryProps {}

const FilterSummary: React.FC<FilterSummaryProps> = props => {
  return (
    <FilterSummaryContainer>
      <Header>Filter Details</Header>
      <SummaryContainer>
        <ActionsContainer>
          <FixedWidthButton variant="filled" color="orange">
            Update Filter
          </FixedWidthButton>
          <FixedWidthButton variant="filled" color="blue">
            Actions
          </FixedWidthButton>
        </ActionsContainer>
        <SummaryTotals>
          <TotalContainer>
            <P>Balance:</P>
            <P>$966,088,846</P>
          </TotalContainer>
          <TotalContainer>
            <P>Count:</P>
            <P>55,196</P>
          </TotalContainer>
        </SummaryTotals>
        <FilterDetails />
      </SummaryContainer>
    </FilterSummaryContainer>
  );
};

const FilterDetailsContainer = styled.div`
  padding-top: 5px;
  border-top: 1px dashed black;
`;

const FilterDetails: React.FC = () => {
  const { values, update } = useTabValues();
  return (
    <FilterDetailsContainer>
      <FilterPart values={values} onClick={(key) => update(key, [])} />
    </FilterDetailsContainer>
  );
};

const FilterPartHeader = styled.div`
  height: 45px;
  margin-bottom: 5px;
  background-color: rgb(243, 119, 34, 0.2);
  display: grid;
  grid-template-columns: auto auto;
  justify-content: space-between;
  align-content: center;
  padding: 0 10px;
  width: 100%;
  box-sizing: border-box;
  span {
    color: #f37722;
    font-size: 1.5em;
  }
`;

const FilterPartContainer = styled.div`
  display: grid;
  grid-row-gap: 5px;
`;

const FilterField = styled.div`
  background-color: white;
  padding: 8px 20px 8px 5px;
  border: solid 2px #a09c92;
  display: flex;
  align-items: center;
  vertical-align: middle;
  text-align: left;
  margin-bottom: 10px;
`;

const FilterFieldContent = styled.div`
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  width: 100%;
  color: #808080;
`;

const Bold = styled.span`
  font-weight: bold;
`;

interface FilterPartProps {
  values: Record<string, string[]>;
  onClick: (key: string) => void;
}

const FilterPart: React.FC<FilterPartProps> = ({ values, onClick }) => {
  return (
    <FilterPartContainer>
      <FilterPartHeader>
        <span>Filter 1</span>
        <FontAwesomeIcon icon={faPencilAlt} color="#F37722" size="lg" />
      </FilterPartHeader>
      {Object.entries(values).filter(entry => entry[1].length > 0).map(entry => (
        <FilterField>
          <FilterFieldContent>
            <Bold>{entry[0]} </Bold>
            {entry[1].filter(e => e.length > 0).join(', ')}
          </FilterFieldContent>
          <span css={{ cursor: 'pointer' }} onClick={() => onClick(entry[0])}>
            <FontAwesomeIcon icon={faTimes} color="rgb(128, 128, 128)" />
          </span>
        </FilterField>
      ))}
    </FilterPartContainer>
  );
};

export { FilterSummary };
