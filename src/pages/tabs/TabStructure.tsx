import React, { useState } from 'react';
import {
  Tab,
  TabGroup,
  TabPanel,
  PanelSectionHeader,
  SelectDeselectAll,
  CheckBox,
  TextInput,
  Select,
  Option
} from '../../components/index';
import styled from '@emotion/styled';
import { Global, css } from '@emotion/core';
import {
  TabType,
  TextField,
  SelectField,
  CheckboxField,
  LookbackField
} from './tabJson';
import { TabValuesProvider, useTabValues } from './TabValuesContext';

interface TabStructureProps {
  tabJson: TabType[];
  values: Record<string, string[]>;
}

const CheckBoxesContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  padding-bottom: 15px;
  grid-column-gap: 30px;
`;

const TextInputContainer = styled.div`
  display: grid;
  grid-auto-flow: column;
  grid-column-gap: 15px;
  padding-bottom: 15px;
`;

const OuterTextInputContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  padding-bottom: 15px;
  grid-column-gap: 30px;
`;

const TabStructure: React.FC<TabStructureProps> = ({ tabJson, values }) => {
  const [primaryTabIndex, setPrimaryTabIndex] = useState(0);
  const [secondaryTabIndex, setSecondaryTabIndex] = useState(0);
  const primaryTabs = tabJson.map(tab => tab.name);
  const secondaryTabs = tabJson[primaryTabIndex].subTabs.map(tab => tab.name);
  const { fields } = tabJson[primaryTabIndex].subTabs[secondaryTabIndex];
  return (
    <div>
      <Global
        styles={css`
          * {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 13px;
          }
        `}
      />
      <TabGroup
        selectedIndex={primaryTabIndex}
        onChange={i => {
          setSecondaryTabIndex(0);
          setPrimaryTabIndex(i);
        }}
        borderBottom={false}
      >
        {primaryTabs.map(tab => (
          <Tab key={tab}>{tab}</Tab>
        ))}
      </TabGroup>
      <TabGroup
        selectedIndex={secondaryTabIndex}
        onChange={setSecondaryTabIndex}
      >
        {secondaryTabs.map(tab => (
          <Tab key={tab}>{tab}</Tab>
        ))}
      </TabGroup>
      <TabPanel>
        <Fields fields={fields} />
      </TabPanel>
      <Values />
    </div>
  );
};

const Values = () => {
  const { values } = useTabValues();
  return <div>{JSON.stringify(values)}</div>;
};

interface FieldsProps {
  fields: (CheckboxField | TextField | SelectField | LookbackField)[];
}

const Fields: React.FC<FieldsProps> = ({ fields }) => {
  const groups: (
    | CheckboxField
    | TextField
    | SelectField
    | LookbackField)[][] = fields.reduce((acc, curr) => {
    if (
      acc.length === 0 ||
      acc[acc.length - 1][acc[acc.length - 1].length - 1].type !== curr.type
    ) {
      return [...acc, [curr]];
    } else {
      acc[acc.length - 1].push(curr);
      return acc;
    }
  }, []);
  return (
    <>
      {groups.map((group, i) => {
        switch (group[0].type) {
          case 'checkbox':
            return (
              <CheckBoxSection key={i} fields={group as CheckboxField[]} />
            );
          case 'select':
            return <SelectSection key={i} fields={group as SelectField[]} />;
          case 'text':
            return <TextInputSection key={i} fields={group as TextField[]} />;
          case 'lookback':
            return (
              <LookbackSection key={i} fields={group as LookbackField[]} />
            );
        }
      })}
    </>
  );
};

interface CheckboxSectionProps {
  fields: CheckboxField[];
}

const CheckBoxSection: React.FC<CheckboxSectionProps> = ({ fields }) => {
  const { values, update } = useTabValues();
  const getValue = (field, option) => {
    return (
      values[field.name] !== undefined &&
      values[field.name].includes(option.value)
    );
  };
  const updateValue = (
    field: CheckboxField,
    option: { label: string; value: string }
  ) => {
    let vals = values[field.name] || [];
    if (vals.includes(option.value)) {
      vals = vals.filter(v => v !== option.value);
    } else {
      vals = [...vals, option.value];
    }
    update(field.name, vals);
  };

  const updateAll = (field: CheckboxField, value: boolean) => {
    if (!value) {
      return update(field.name, []);
    } else {
      return update(field.name, field.options.map(options => options.value));
    }
  };
  return (
    <>
      {fields.map((field, i) => (
        <React.Fragment key={i}>
          <PanelSectionHeader text={field.name} />
          <SelectDeselectAll
            onSelect={() => updateAll(field, true)}
            onDeselect={() => updateAll(field, false)}
          />
          <CheckBoxesContainer>
            {field.options!.map(option => (
              <CheckBox
                label={option.label}
                checked={getValue(field, option)}
                onChange={() => updateValue(field, option)}
                key={option.value}
              />
            ))}
          </CheckBoxesContainer>
        </React.Fragment>
      ))}
    </>
  );
};

interface TextSectionProps {
  fields: TextField[];
}

const TextInputSection: React.FC<TextSectionProps> = ({ fields }) => {
  const { values, update } = useTabValues();
  const updateValue = (name, i, value, numInputs) => {
    let oldVals = values[name];
    if (!oldVals) {
      oldVals = [];
      for (let i = 0; i < numInputs; i++) {
        oldVals.push('');
      }
    }
    const vals = [...oldVals];
    vals[i] = value;
    update(name, vals);
  };
  return (
    <OuterTextInputContainer>
      {fields.map(({ name, inputs }) => (
        <span key={name}>
          <PanelSectionHeader text={name} />
          <TextInputContainer>
            {inputs.map((input, i) => (
              <TextInput
                value={values[name] ? values[name][i] : ''}
                key={i}
                placeholder={input.placeHolder}
                valueType={input.valueType}
                onChange={val => updateValue(name, i, val, inputs.length)}
              />
            ))}
          </TextInputContainer>
        </span>
      ))}
    </OuterTextInputContainer>
  );
};

interface LookbackSectionProps {
  fields: LookbackField[];
}

const LookbackSection: React.FC<LookbackSectionProps> = ({ fields }) => {
  const { values, update } = useTabValues();
  const updateValue = (name, i, value, numInputs) => {
    let oldVals = values[name];
    if (!oldVals) {
      oldVals = [];
      for (let i = 0; i < numInputs; i++) {
        oldVals.push('');
      }
    }
    const vals = [...oldVals];
    vals[i] = value;
    update(name, vals);
  };
  return (
    <OuterTextInputContainer>
      {fields.map(({ name, inputs, options }) => (
        <span key={name}>
          <PanelSectionHeader text={name} />
          <TextInputContainer>
            {inputs.map((input, i) => (
              <TextInput
                value={values[name] ? values[name][i] : ''}
                key={i}
                placeholder={input.placeHolder}
                valueType={input.valueType}
                onChange={val => updateValue(name, i, val, inputs.length + 1)}
              />
            ))}
            <Select
              value={values[name] && values[name][inputs.length + 1]}
              onChange={e =>
                updateValue(
                  name,
                  inputs.length,
                  e.currentTarget.value,
                  inputs.length + 1
                )
              }
            >
              {options.map(option => (
                <Option
                  key={option.value}
                  label={option.label}
                  value={option.value}
                >
                  {option.label}
                </Option>
              ))}
            </Select>
          </TextInputContainer>
        </span>
      ))}
    </OuterTextInputContainer>
  );
};

interface SelectSectionProps {
  fields: SelectField[];
}

const SelectSection: React.FC<SelectSectionProps> = ({ fields }) => {
  const { values, update } = useTabValues();
  return (
    <>
      {fields.map(field => (
        <React.Fragment key={field.name}>
          <PanelSectionHeader text={field.name} />
          <CheckBoxesContainer>
            <Select
              value={values[field.name] && values[field.name][0]}
              onChange={e => update(field.name, [e.target.value])}
            >
              {field.options.map(option => (
                <Option
                  key={option.value}
                  label={option.label}
                  value={option.value}
                >
                  {option.label}
                </Option>
              ))}
            </Select>
          </CheckBoxesContainer>
        </React.Fragment>
      ))}
    </>
  );
};

export { TabStructure };
