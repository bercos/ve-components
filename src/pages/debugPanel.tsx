import React, { useState } from 'react';
import { render } from 'react-dom';
import styled from '@emotion/styled';
import { DebugPanel } from './debugPanel/DebugPanel';

render(
  <DebugPanel />,
  document.getElementById('root')
);
