const HtmlWebpackPlugin = require('html-webpack-plugin');
const fs = require('fs');
const path = require('path');

const PAGES_DIR = path.join(__dirname, '..', 'src', 'pages'); // Directory to look for files to create bundles from
const PAGES_REGEX = /.tsx?$/; // All pages must end in .ts or .tsx

// Get bundle entry points
const getEntries = () => {
  const entries = {};
  const files = fs.readdirSync(PAGES_DIR);
  files
    .filter(filename => PAGES_REGEX.test(filename))
    .forEach(file => {
      const name = file.replace(PAGES_REGEX, ''); // name is filename without extension
      const filePath = path.join(PAGES_DIR, file); // compute path
      entries[name] = filePath; // { name: path }
    });
  return entries;
};

// Create html page per entry
const getHtmlWebpackPlugins = (entries, mode) => {
  // Check if we are running webpack-dev-server
  const isDevServer = process.argv.find(v => v.includes('webpack-dev-server'));
  return Object.keys(entries).map(
    key =>
      new HtmlWebpackPlugin({
        template: mode === 'development' ? './index.html' : './fragment.html',
        chunks: [key, 'react', 'runtime'],
        filename: `${key}.html`
      })
  );
};

module.exports = {
  getEntries,
  getHtmlWebpackPlugins
};
