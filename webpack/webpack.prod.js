const merge = require('webpack-merge');
const common = require('./webpack.common');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { getHtmlWebpackPlugins } = require('./webpack.utils');

module.exports = merge(common, {
  mode: 'production',
  plugins: [
    new CleanWebpackPlugin(),
    ...getHtmlWebpackPlugins(common.entry, 'production')
  ]
});
