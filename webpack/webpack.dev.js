const merge = require('webpack-merge');
const common = require('./webpack.common');
const { getHtmlWebpackPlugins } = require('./webpack.utils');

module.exports = merge(common, {
  // include source maps
  module: {
    rules: [{ enforce: 'pre', test: /\.js$/, use: 'source-map-loader' }]
  },
  mode: 'development',
  devtool: 'inline-source-map',
  // webpack-dev-server
  devServer: {
    open: true,
    openPage: 'table.html'
  },
  plugins: getHtmlWebpackPlugins(common.entry, 'development')
});
