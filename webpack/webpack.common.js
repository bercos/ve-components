const path = require('path');
const { getEntries } = require('./webpack.utils');
const BundleAnalyzer = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = {
  entry: getEntries(),
  output: {
    // content hash in the name allows the 
    // browser to know when to invalidate its caches
    filename: '[name].[contenthash].js', 
    path: path.join(__dirname, '..', 'dist')
  },
  // Split our bundles into several chunks so that
  // while our source may change often the library
  // code may not and it can stay cached
  optimization: {
    // content hash based on relative file path 
    // instead of import order
    moduleIds: 'hashed',
    // chunk for webpack runtime
    runtimeChunk: 'single',
    splitChunks: {
      cacheGroups: {
        // chunk for react and react-dom
        react: {
          test: /[\\/]node_modules[\\/](react|react-dom)[\\/]/,
          name: 'react',
          chunks: 'all'
        }
      }
    }
  },
  module: {
    rules: [
      // use babel for typescript files
      {
        test: /(\.tsx?)|(\.jsx?)$/,
        use: 'babel-loader',
        exclude: /node_modules/
      },
      // include css files
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      }
    ]
  },
  // bundle any tsx, ts, js, or css files
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.css'],
    alias: {
      "@components": path.join(__dirname, '..', 'src', 'components')
    }
  },
  // hide bundle size warnings
  performance: {
    hints: false
  }
};
