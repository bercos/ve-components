import { configure, addDecorator } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';

addDecorator(withKnobs);

const req = require.context('../stories', true, /stories.tsx$/);

function loadStories() {
  req.keys().forEach(req);
}

configure(loadStories, module);
