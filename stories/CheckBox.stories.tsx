import React from 'react';
import { storiesOf } from '@storybook/react';
import { CheckBox } from '../src/components';

const CheckBoxWithState: React.FC<{ label: string; checked: boolean }> = ({
  label,
  checked
}) => {
  const [isChecked, setChecked] = React.useState(checked);
  return (
    <CheckBox
      checked={isChecked}
      onChange={() => setChecked(!isChecked)}
      label={label}
    />
  );
};

storiesOf('CheckBox', module)
  .add('checked', () => (
    <CheckBoxWithState label="Standard Category" checked={true} />
  ))
  .add('unchecked', () => (
    <CheckBoxWithState label="Standard Category" checked={false} />
  ));
