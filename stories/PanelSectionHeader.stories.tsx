import React from 'react';
import { storiesOf } from '@storybook/react';
import { PanelSectionHeader } from '../src/components';

storiesOf('PanelSectionHeader', module).add(
  'with text Standard Category',
  () => <PanelSectionHeader text="Standard Category" />
);
