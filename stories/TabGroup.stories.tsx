import React from 'react';
import { storiesOf } from '@storybook/react';
import { TabGroup, Tab } from '../src/components';

const nums = [];
for (let i = 0; i < 10; i++) {
  nums.push(i);
}

const TabGroupWithState = ({ dark, borderBottom }) => {
  const [selected, setSelected] = React.useState(0);
  return (
    <TabGroup
      dark={dark}
      borderBottom={borderBottom}
      selectedIndex={selected}
      onChange={val => setSelected(val)}
    >
      {nums.map(i => (
        <Tab>{i}</Tab>
      ))}
    </TabGroup>
  );
};

storiesOf('TabGroup', module)
  .add('with border bottom', () => (
    <TabGroupWithState dark={false} borderBottom={true} />
  ))
  .add('without border bottom', () => (
    <TabGroupWithState dark={false} borderBottom={false} />
  ))
  .add('with border bottom and dark', () => (
    <TabGroupWithState dark={true} borderBottom={true} />
  ))
  .add('without border bottom and dark', () => (
    <TabGroupWithState dark={true} borderBottom={false} />
  ))
  .add('stacked', () => (
    <span>
      <TabGroupWithState dark={false} borderBottom={false} />
      <TabGroupWithState dark={true} borderBottom={true} />
    </span>
  ));
