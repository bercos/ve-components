import React from 'react';
import { storiesOf } from '@storybook/react';
import { ElementHeader } from '../src/components';
import { action } from '@storybook/addon-actions';

storiesOf('ElementHeader', module).add('with title Table', () => (
  <ElementHeader title="Table" onClick={action('Clicked')} />
));
