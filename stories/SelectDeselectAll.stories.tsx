import React from 'react';
import { storiesOf } from '@storybook/react';
import { SelectDeselectAll } from '../src/components';

storiesOf('SelectDeselectAll', module).add('standard', () => (
  <SelectDeselectAll
    onSelect={() => console.log('Select All')}
    onDeselect={() => console.log('Deselect All')}
  />
));
