import React from 'react';
import { storiesOf } from '@storybook/react';
import { Button } from '../src/components';
import { select } from '@storybook/addon-knobs';
const variantOptions: { outlined: 'outlined'; filled: 'filled' } = {
  outlined: 'outlined',
  filled: 'filled'
};

const colorOptions: { orange: 'orange'; blue: 'blue' } = {
  orange: 'orange',
  blue: 'blue'
};

storiesOf('Button', module)
  .add('Interactive', () => (
    <Button
      variant={select<'outlined' | 'filled'>(
        'Variant',
        variantOptions,
        'outlined'
      )}
      color={select<'orange' | 'blue'>('Color', colorOptions, 'blue')}
    >
      Actions
    </Button>
  ))
  .add('outlined with color blue', () => (
    <Button variant="outlined" color="blue">
      Actions
    </Button>
  ))
  .add('outlined with color orange', () => (
    <Button variant="outlined" color="orange">
      Actions
    </Button>
  ))
  .add('filled with color blue (Default)', () => <Button>Actions</Button>)
  .add('filled with color orange', () => (
    <Button color="orange">Actions</Button>
  ));
