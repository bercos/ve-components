import React from 'react';
import { storiesOf } from '@storybook/react';
import { Select, Option } from '../src/components';
import { boolean } from '@storybook/addon-knobs';

const nums = [];
for (let i = 0; i < 10; i++) {
  nums.push(i);
}

const SelectWithState = ({ value, children, ...props }) => {
  const [selected, setSelected] = React.useState(value);
  return (
    <Select
      value={selected}
      onChange={e => setSelected(e.currentTarget.value)}
      {...props}
    >
      {children}
    </Select>
  );
};

storiesOf('Select', module)
  .add('with none disabled', () => (
    <SelectWithState value={0}>
      {nums.map(i => (
        <Option disabled={boolean(`${i} Disabled`, false)} value={i}>
          {i}
        </Option>
      ))}
    </SelectWithState>
  ))
  .add('with "1" disabled', () => (
    <SelectWithState value={0}>
      {nums.map(i => (
        <Option disabled={i === 1} value={i}>
          {i}
        </Option>
      ))}
    </SelectWithState>
  ));
